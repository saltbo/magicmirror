ARG buildarch
ARG NODE_VERSION
FROM ${buildarch}node:${NODE_VERSION}-buster-slim as builder

USER root

WORKDIR /opt/magic_mirror

RUN set -e; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get -qy --no-install-recommends install git ca-certificates; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*; \
    chown -R node:node .;

USER node

ARG BuildRef
ARG GitRepo
ARG buildarch
RUN set -e; \
    node -v; \
    echo BuildRef="${BuildRef}"; \
    echo GitRepo="${GitRepo}"; \
    git clone --depth 1 -b "${BuildRef}" --single-branch "${GitRepo}" .; \
    git log -1; \
    npmargs="--only=prod"; \
    [ -z "${buildarch}" ] && npmargs="${npmargs} --no-optional"; \
    if [ $(echo "${BuildRef}" | grep -E "^v[0-9]+.[0-9]+.[0-9]+$") ] ; then \
      npm ci ${npmargs}; \
    else \
      npm install ${npmargs}; \
    fi; \
    cat package.json; \
    npm cache clean -f; \
    rm -rf /home/node/.cache; \
    sed -i "s:address\: \"localhost\":address\: \"0.0.0.0\":" config/config.js.sample; \
    sed -i "s:ipWhitelist\: \[.*\],:ipWhitelist\: \[\],:" config/config.js.sample; \
    mkdir mount_ori; \
    mv modules mount_ori/; \
    mv config mount_ori/; \
    mv css mount_ori/; \
    # remove unused test stuff which causes CVE's:
    rm -rf node_modules/clarinet/benchmark;

FROM scratch
LABEL maintainer="Karsten Hassel"

COPY --from=builder /opt/magic_mirror /opt/magic_mirror
